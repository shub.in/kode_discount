from apps.discounts.views import DiscountViewSet
from apps.products.views import ProductViewSet

from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'discounts', DiscountViewSet)

urlpatterns = router.urls
