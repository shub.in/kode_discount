from django.db import models
from django.utils.translation import ugettext_lazy as _


class Product(models.Model):
    DEFAULT_IMAGE = 'https://loremflickr.com/320/320/gadget'
    title = models.CharField(_('Title'), max_length=255)
    description = models.TextField(_('Description'), null=True, blank=True)
    price = models.DecimalField(_('Price'), max_digits=8, decimal_places=2)
    image = models.URLField(_('Image URL'), null=True, blank=True,
                            default=DEFAULT_IMAGE,
                            help_text=_('Image URL of product'))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')
        ordering = ['title']
