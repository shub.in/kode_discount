from apps.discounts.models import Discount

from rest_framework import serializers

from .models import Product


class DiscountSerializer(serializers.ModelSerializer):
    result = serializers.DecimalField(max_digits=8, decimal_places=2,
                                      coerce_to_string=False,
                                      read_only=True)
    cashback = serializers.DecimalField(max_digits=8, decimal_places=2,
                                        coerce_to_string=False,
                                        read_only=True)

    class Meta:
        model = Discount
        exclude = ['users', 'products']


class ProductListSerializer(serializers.ModelSerializer):
    discounts = DiscountSerializer(many=True)
    price = serializers.DecimalField(max_digits=8, decimal_places=2,
                                     coerce_to_string=False)

    class Meta:
        model = Product
        fields = ['id', 'title', 'price', 'image', 'discounts']


class ProductItemSerializer(serializers.ModelSerializer):
    discounts = DiscountSerializer(many=True)
    price = serializers.DecimalField(max_digits=8, decimal_places=2,
                                     coerce_to_string=False)

    class Meta:
        model = Product
        fields = ['id', 'title', 'description', 'price', 'image',
                  'discounts']
