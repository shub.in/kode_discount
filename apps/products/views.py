from datetime import datetime

from apps.discounts.models import Discount

from django.db.models import Prefetch, Q
from django.shortcuts import get_object_or_404

from rest_framework import viewsets
from rest_framework.response import Response

from .models import Product
from .serializers import ProductItemSerializer, ProductListSerializer


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """
    retrieve:
    ### Return the given product.
    Product contain a list of discounts
    where **result** is  price of product with a discount
    and **cashback** is amount of money which will be return after
    the purchase.

    list:
    ### Return a list of all the products.
    Product contain a list of discounts
    where **result** is price of product with a discount
    and **cashback** is amount of money which will be return after
    the purchase.
    """
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer
    serializer_class_item = ProductItemSerializer

    def get_serializer(self, *args, **kwargs):
        if self.action == 'retrieve':
            return ProductItemSerializer(*args, **kwargs)
        return super().get_serializer(*args, **kwargs)

    def prepare_queryset(self, request):
        if request.user.is_authenticated:
            return self.get_queryset().prefetch_related(
                Prefetch('discounts', Discount.objects.filter(
                    date_start__lte=datetime.now(),
                    date_end__gte=datetime.now())
                         .exclude(~Q(users__in=[request.user]), type='PE')))
        else:
            return self.get_queryset().prefetch_related(
                Prefetch('discounts', Discount.objects.filter(
                    date_start__lte=datetime.now(),
                    date_end__gte=datetime.now())
                         .exclude(type='PE')))

    def calculate_discount(self, product):
        for discount in product.discounts.all():
            discount_amount = product.price * discount.percent / 100
            if discount.is_cashback:
                discount.cashback = discount_amount
                discount.result = product.price
            else:
                discount.result = product.price - discount_amount

    def list(self, request, *args, **kwargs):
        queryset = self.prepare_queryset(request)
        for product in queryset:
            self.calculate_discount(product)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, *args, **kwargs):
        queryset = self.prepare_queryset(request)
        product = get_object_or_404(queryset, pk=pk)
        self.calculate_discount(product)
        serializer = self.get_serializer(product)
        return Response(serializer.data)
