from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Discount


class DiscountForm(forms.ModelForm):
    class Meta:
        model = Discount
        exclude = ['is_cashback']

    def clean(self):
        cleaned_data = super().clean()
        date_start = cleaned_data.get('date_start')
        date_end = cleaned_data.get('date_end')
        if date_start and date_end:
            if date_start >= date_end:
                self.add_error('date_start',
                               _('Start date should be earlier '
                                 'than end date.'))
                self.add_error('date_end',
                               _('End date must be later than '
                                 'the start date.'))
                raise forms.ValidationError(_('Error while adding '
                                              'a discount.'))
        return cleaned_data
