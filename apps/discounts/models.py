from apps.products.models import Product

from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Discount(models.Model):
    TYPES = (
        ('SI', _('Simple')),
        ('AF', _('Affiliate')),
        ('PE', _('Personal')),
    )

    title = models.CharField(_('Title'), unique=True, max_length=128)
    date_start = models.DateField(_('Start date'))
    date_end = models.DateField(_('End date'))
    is_cashback = models.BooleanField(_('Is cashback?'), default=False)
    type = models.CharField(_('Type'), choices=TYPES, default='SI',
                            max_length=2)
    percent = models.PositiveSmallIntegerField(_('Percent'), default=10,
                                               validators=[
                                                   MinValueValidator(1),
                                                   MaxValueValidator(99)])

    products = models.ManyToManyField(Product, blank=True,
                                      related_name='discounts')
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True,
                                   related_name='discounts')

    def __str__(self):
        return '{} ({})'.format(self.title, self.percent)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.type in ['AF']:
            self.is_cashback = True
        super().save(force_insert=False, force_update=False, using=None,
                     update_fields=None)
