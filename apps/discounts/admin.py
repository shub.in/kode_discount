from django.contrib import admin

from .forms import DiscountForm
from .models import Discount


@admin.register(Discount)
class DiscountAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'type', 'percent', 'date_start',
                    'date_end', 'is_cashback']
    search_fields = ['title']
    list_filter = ['type', 'is_cashback']
    filter_horizontal = ('products', 'users')
    form = DiscountForm
