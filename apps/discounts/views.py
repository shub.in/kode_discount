from datetime import datetime

from django.db.models import Q
from django.shortcuts import get_object_or_404

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.schemas import AutoSchema

from .models import Discount
from .serializers import DiscountItemSerializer, DiscountListSerializer


class DiscountViewSet(viewsets.ReadOnlyModelViewSet):
    """
    retrieve:
    ### Return the given discount.
    Product contain fields **result** and **cashback**
    where **result** is price of product with a discount
    and **cashback** is amount of money which will be return after
    the purchase.


    list:
    ### Return a list of the all current discounts.
    Product contain fields **result** and **cashback**
    where **result** is price of product with a discount
    and **cashback** is amount of money which will be return after
    the purchase.
    """
    queryset = Discount.objects.filter(
            date_start__lte=datetime.now(), date_end__gte=datetime.now())
    serializer_class = DiscountListSerializer
    schema = AutoSchema()

    def get_serializer(self, *args, **kwargs):
        if self.action == 'retrieve':
            return DiscountItemSerializer(*args, **kwargs)
        return super().get_serializer(*args, **kwargs)

    def prepare_queryset(self, request):
        if request.user.is_authenticated:
            return self.get_queryset() \
                .exclude(~Q(users__in=[request.user]), type='PE')
        else:
            return self.get_queryset().exclude(type='PE')

    def list(self, request, *args, **kwargs):
        queryset = self.prepare_queryset(request)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, *args, **kwargs):
        queryset = self.prepare_queryset(request).prefetch_related('products')
        discount = get_object_or_404(queryset, pk=pk)
        for product in discount.products.all():
            discount_amount = product.price * discount.percent / 100
            if discount.is_cashback:
                product.cashback = discount_amount
                product.result = product.price
            else:
                product.result = product.price - discount_amount
        serializer = self.get_serializer(discount)
        return Response(serializer.data)
