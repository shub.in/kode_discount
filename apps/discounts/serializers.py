from apps.products.models import Product

from rest_framework import serializers

from .models import Discount


class ProductSerializer(serializers.ModelSerializer):
    price = serializers.DecimalField(max_digits=8, decimal_places=2,
                                     coerce_to_string=False)
    result = serializers.DecimalField(max_digits=8, decimal_places=2,
                                      coerce_to_string=False,
                                      read_only=True)
    cashback = serializers.DecimalField(max_digits=8, decimal_places=2,
                                        coerce_to_string=False,
                                        read_only=True, default=0)

    class Meta:
        model = Product
        fields = ['id', 'title', 'price', 'image', 'result', 'cashback']


class DiscountListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discount
        exclude = ['users', 'products']


class DiscountItemSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta:
        model = Discount
        exclude = ['users']
