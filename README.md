# Test task for Backend developer

### What do you need:
1. Implement a REST API service
2. Provide spec API services implemented
3. Provide project code in git repository
4. Expand implemented API for demonstration, or make launch possible
service using docker/docker-compose.

### Project stack:
* Python or Go
* Allowed to use Django, Flask, Tornado
* Database - Postgres

### A task:
A chain of stores is developing an application to notify loyal users about current promotions and discounts.
Shares are of the following types:
* Simple (SI): for the duration of the promotion, the selected products are provided various discounts.
* Affiliate (AF): for the duration of the promotion when buying certain goods on Buyer's bank card returns a certain part of the cost purchases.
* Personal (PE): for the duration of the promotion for certain products for some Buyers have a certain discount.

*When developing, assume that requests come on behalf of an already authorized the user whose session identifier is passed in the cookie.*

### Deploying with docker-compose

```bash
$ git clone git@gitlab.com:shub.in/kode_discount.git
$ cd kode_discount
$ docker-compose up --build
```

Username: **admin** 
Password: **admin** 
*Some discounts are available only for authorized users.*
