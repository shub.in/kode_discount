FROM python:alpine
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
RUN apk add gcc musl-dev postgresql-dev
ADD req.*.txt /code/
RUN pip install -r req.dev.txt
ADD . /code/
