from django.shortcuts import render


def home(request):
    return render(request, 'kode_discount/home.html')
